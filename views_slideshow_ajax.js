// $Id: views_slideshow_ajax.js,v 1.1 2011/02/02 10:10:36 kentr Exp $

Drupal.behaviors.viewsSlideshowAjaxLoad = function (context) {
  var key;
  for (key in Drupal.settings.viewsSlideshowAjax) {
    
    // $meta is the array of Views Slideshow Ajax $meta information
    // about the slideshow.
    var $meta = Drupal.settings.viewsSlideshowAjax[key];
    
    // build array of loader functions to re-execute.  Use context param
    // to isolate to our slideshows.

    var view = $($meta['slideshow_view_dom_id']);
    
    /*
     * Store the meta ID with the slideshow object to facilitate later retrieval.
     */
    var slideshow = $($meta['slideshow_full_id'] );
    slideshow.data('settings_key', key);
    
    /*
     * Get the slide count from the response for real-time addition of slides.
     */
    slideshow.ajaxComplete(function(e, request, options) {
          var response = Drupal.parseJson(request.responseText);
          if (typeof response.slideshow_count == 'number') {
            // views_slideshow_ajax_log(response.slideshow_count);
            Drupal.settings.viewsSlideshowAjax[$(this).data('settings_key')].slideshow_count = response.slideshow_count;
          }
        }
    );
    /**
     * Add the extra containers to the items.
     * 
     * Assumes that the container for the slideshow element is the first
     * child of the main slideshow element.
     * 
     * @see views_slideshow.js
     */

    // $cont is the immediate container of the slideshow elements.
    var $cont = $($meta['slideshow_full_id'] + ' :first');

    // add the preload elements
    $cont.append('<div class=" views-slideshow-preload p0" style="display:none; position: absolute; left: -999999"></div>').data('settings_key', key);
    $cont.append('<div class="views-slideshow-preload p1" style="display:none; position: absolute; left: -999999"></div>');
    
    // for theming
    $cont.addClass('views-slideshow-ajax');
    delete $cont;
    
    // Initiate the load for the next slide
    
    // remove the "processed" class
    processedClass = $meta['mode_name'] + '-processed';
    view.find('.' + processedClass).toggleClass(processedClass);
    delete view;

    /*
     * settings is the object of Views Slideshow options, from the view edit form.
     * We're assuming that it's stored in Drupal.settings[$meta['mode_name']]...
     * 
     * Works on:
     *   * FF 3.6 Mac
     *   * Safari 5 Mac
     *   * Chrome 9.0.597.102 Mac
     */ 
    var $settings = Drupal.settings[$meta['mode_name']][$meta['slideshow_full_id']];

    /*
     * Add before / after callbacks to preload.
     */
    if (typeof $settings != 'object') {
      views_slideshow_ajax_log("Settings not found for " + $meta['slideshow_full_id']);
      continue;
    }

    // opts is the array of jQuery cycle options.
    var opts = $settings.opts;

    // Hijack the "before" method so we can customize the handling.
    opts.beforeOrig = opts.before;
    opts.before = function(curr, next, opts) {
      var $meta = Drupal.settings.viewsSlideshowAjax['#' + $(opts.$cont).parent().attr('id')];

      // debugger;
      if ($meta.isPrevEvent) {
       $meta.isPrevEvent = false;

        --$meta.current_item;
        if ($meta.current_item < 0) {
          $meta.current_item = $meta.slideshow_count + $meta.current_item;
        }
        
        var url = $meta.ajax_url + $meta.current_item ;
        // Target of the next preload
        var preloadTarget = (opts.currSlide + opts.slideCount - 1) % opts.slideCount;
        /**
         * @todo: error checking before progressing.
         * @todo: Move the rest of this out of the If condition.
         */
        views_slideshow_ajax_preload(opts.elements[preloadTarget], url);
        var t = $(opts.elements[preloadTarget]);

        opts.beforeOrig(curr, next, opts);
        return;
      }

	      // targetId hasn't been set yet, so assume it's the first parent.
	      
	      // Gets called at very beginning of slideshow, so skip preload
	      if ($meta.first_cycle) {
	        $meta.first_cycle = 0;
	        return;
	      }
	
	      // Target of the next preload
	      var preloadTarget = (opts.currSlide + opts.slideCount - 1) % opts.slideCount;
	
	      var url = $meta.ajax_url + (($meta.current_item + opts.slideCount - 1) % $meta.slideshow_count);
	      
	      /**
	       * @todo: error checking before progressing.
	       */
	      views_slideshow_ajax_preload(opts.elements[preloadTarget], url);
	      
	      /*
	       * looping current_item to avoid problems with long running shows
	       * & out of range errors
	       */
	      $meta.current_item = (($meta.current_item + 1) % $meta.slideshow_count);
	
	      opts.beforeOrig(curr, next, opts);	
	      
	      delete result;
	      delete $meta;
      };
    
    opts.onPrevNextEvent = function(isNext, zeroBasedSlideIndex, slideElement) {
      /*
       * Could do the preloading here for marginal reduction in lag time, but still
       * must test for "previous" event in before() method to skip the preloading
       * of _other_ elements (so that we can go forward again without them
       * having changed). 
       */
      if (!isNext) {
        // add ".loading" class to incoming slide for theming
        $(slideElement).addClass('loading');
        /*
         * Set flag for before() method
         * This is going into the ajax "meta" info for the slideshow
         * in Drupal.settings.viewsSlideshowAjax[]
         */
        Drupal.settings.viewsSlideshowAjax[$(slideElement).parent().data('settings_key')].isPrevEvent = !isNext;
      }
    }
 
    /* 
     * Now that we've added empty children, call the respective 
     * Drupal.behaviors method again to initiate the slideshow.
     */

      if ($settings.start_paused) {
        
        var startPaused = meta.mode_name + 'Pause';
        if ($.isFunction(window[startPaused])) {
          //debugger;
          window[startPaused]($settings);
        }
        else {
          views_slideshow_ajax_log('Error starting slideshow in paused mode: ' + $settings.vss_id);
        }
        /*
        try {
          debugger;
          startPaused($settings);
        }
        catch (e) {
          views_slideshow_ajax_log('Error starting slideshow in paused mode: ' + $settings.vss_id);
        }
        */
      }
      else {
        // Instantiate and start cycle
        $($settings.targetId).cycle($settings.opts);
      }
    /*
     * Populate the preload elements.
     * Done after initiating the slideshow to avoid delays.
     * 
     * We're assuming that we're starting at the beginning.
     * To start at the last slide viewed, change the '1' below as
     * needed to call the proper page. 
     * 
     * If synchronizing becomes a problem, this may need to be moved
     * above the slideshow init, or perhaps a config option added to
     * fire this before the init.
     */

    views_slideshow_ajax_preload($('.views-slideshow-preload', $cont)[0], $meta['ajax_url'] + '1');
    
    delete opts;
    delete result;
  }
}

/**
 * Load a JSON result into a DOM preload element.
 *   
 * @param target
 *   A DOM object or jQuery selector into which the result should be
 *   loaded.  The inner HTML of the target will be replaced, leaving
 *   the target itself otherwise unmodified. 
 * @param url
 *   A string representing the full URL for the JSON query.
 *   The JSON response must be an object with a "display" property.
 */
function views_slideshow_ajax_preload(target, url) {
  var json = $.get(
    url,
    function(response) {    	
       var t = $(target);
       var result = Drupal.parseJson(response);
       result = $(result.display);

       t.html(result.html());

       /*
        * Could assume that the slide has the teaser has class '<mode>_hidden',
        * but may not be true...
        */

       t.attr({
         'class': result.attr('class'),
         'id': result.attr('id')
       });
    }
  );
}

function views_slideshow_ajax_log(){if(window.console&&window.console.log){window.console.log("[views slideshow ajax] "+Array.prototype.join.call(arguments," "));}}

/*members Drupal, $, $cont, ajax_url, append, attr, before, beforeOrig, behaviors, 
call, console, currSlide, current_item, cycle, display, elements, find, 
first_cycle, get, html, join, log, mode_name, opts, parent, parseJson, 
prototype, responseText, settings, slideCount, slideshow_count, 
slideshow_full_id, slideshow_view_dom_id, targetId, toggleClass, 
viewsSlideshowAjax, viewsSlideshowAjaxLoad, viewsSlideshowSingleFrame
*/
// $Id: README.txt,v 1.1 2011/02/02 10:10:36 kentr Exp $

INSTALLATION:

1) Copy the folder to the either a standard modules
   directory or the views_slideshow/contrib directory.
2) Enable the Views Slideshow Ajax module under Views at
   admin/build/modules
3) Enable another Views Slideshow sub-module
   (only tested with Singleframe)

USE:

1) Add a display to a new or existing view
    (only tested with block displays).

2) Set the Style of the display to "Slideshow".

3) Choose the slideshow type, as you would for a typical Views Slideshow
    display.

4) In the Style options, click "Use ajax", set other options as desired,
   and save.  

   This will override the regular Views "Use AJAX", "Use pager", and
   "Items per page" settings.  Don't change these settings; they
   are required.

5) Add fields, filters, sorting, etc to the view as desired.

6) Optionally give the block an Admin name under Block settings.

7) Display the block as you would any other: at admin/build/blocks, 
   using Panels, etc.
   
NOTES / ISSUES:

* The views pager won't display.  Anything entered in the views display
  pager preferences is overridden when you save the Style options. I'd say
  that in an ideal world this would be an option, and / or integrated with
  the jQuery Cycle plugin's pager, but I'm not currently in need of this.

* The "Use ajax" item under the style options overrides the normal
  Views "Use AJAX" setting, but I don't think that the revers is true.
  I envision that they would be interchangeable, such that setting either
  one would have the same result.  I'm leaving this as a "feature addition"
   for someone else to implement, or a future version.

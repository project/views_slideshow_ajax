<?php
// $Id: views_slideshow_ajax.views.inc,v 1.1 2011/02/02 10:10:36 kentr Exp $

/**
 * @file
 * Defines the views functionality for Views Slideshow Ajax module.
 */

/**
 * Implements hook_views_slideshow_options_form()
 */
function views_slideshow_ajax_views_slideshow_options_form(&$form, &$form_state, &$view) {
$tmp = '';
  $form['views_slideshow_ajax'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use ajax'),
    '#default_value' => $view->display->handler->use_ajax(),
    '#description' => t('Use ajax loading instead of loading all slides as hidden items.'),
  );
}

/**
 * Implements hook_FORM_ID_alter.
 */
function views_slideshow_ajax_form_views_ui_edit_display_form_alter(&$form, &$form_state) {
  /*
   * Alter presentation of form element.  By default, Views Slideshow
   * wraps additional elements in collapsible fieldsets, and I wasn't keen
   * on that for this feature.
   *
   * If we don't know the mode yet, we can't hook into the mode form
   * because views_slideshow_ajax may be called first.
   *
   * Suppose we could try setting the module weight to make
   * views_slideshow_ajax get called later...
   */
  if (isset($form['style_options']['views_slideshow_ajax'])) {
    unset($form['style_options']['views_slideshow_ajax-prefix']);
    unset($form['style_options']['views_slideshow_ajax-suffix']);
  }
  /*
   * The following line is not working here for some reason. Bug in Views?
   * In hook_form_validate() instead.
   */
  //$form['#submit'][] = 'views_slideshow_ajax_form_views_ui_edit_display_form_submit';
}

/**
 * @todo Look at moving this to a submit handler
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @param unknown_type $view
 */
function views_slideshow_ajax_views_slideshow_options_form_validate(&$form, &$form_state, &$view) {
  /*
   * Add submit handler so we can process our special overrides.
   * Not working in hook_form_alter for some reason.  Bug in Views?
   */
  array_unshift($form_state['submit_handlers'], 'views_slideshow_ajax_views_ui_edit_display_form_submit');
}

/**
 * Custom submit handler for views_ui_edit_display_form
 */
function views_slideshow_ajax_views_ui_edit_display_form_submit(&$form, &$form_state) {

  $display_handler = &$form_state['view']->display[$form_state['display_id']]->handler;

  if ($form_state['values']['style_options']['views_slideshow_ajax'] === 1) {
    /**
     * @todo store override states for the options so all can be restored
     *   if ajax is turned off.
     */
    if (!isset($form_state['values']['style_options']['views_slideshow_ajax_orig'])) {
      // $form_state['values']['style_options']['views_slideshow_ajax_orig'] = 'foo';
    }

    /*
     * use_pager required for views ajax system.
     *
     * Must unset use_pager later to prevent pager rendering, so save user
     * defined value in render_pager.
     */
    if (!isset($display_handler->options['render_pager'])) {
      $display_handler->override_option('render_pager',
        $display_handler->use_pager());
    }
    $display_handler->override_option('use_pager', TRUE);
    $display_handler->override_option('use_ajax', TRUE);
    /*
     * The views ajax response relies on one item per page.
     */
    $display_handler->override_option('items_per_page', 1);

    $form_state['values']['style_options']['mode_name'] = $form['style_options']['mode']['#options'][$form_state['values']['style_options']['mode']];
  }
  else {
    if (isset($form_state['values']['style_options']['views_slideshow_ajax_orig'])) {
      /**
       * @todo restore original values
       */

      unset($form_state['values']['style_options']['views_slideshow_ajax_orig']);
    }
    $display_handler->override_option('use_ajax', FALSE);
  }
}

/**
 * This hook is called right before the render process. The query has
 * been executed, and the pre_render() phase has already happened for
 * handlers, so all data should be available.
 *
 * Adding output to the view can be accomplished by placing text on
 * $view->attachment_before and $view->attachment_after. Altering the
 * content can be achieved by editing the items of $view->result.
 *
 * This hook can be utilized by themes.
 */
function views_slideshow_ajax_views_pre_render(&$view) {
  /*
   * Per note in views_slideshow_ajax_views_slideshow_options_form_validate(),
   * resetting use_pager so pager won't render.
   */
  if (!_is_views_slideshow_ajax($view)) {
    return;
  }
  $view->display_handler->set_option('use_pager', FALSE);
}


function views_slideshow_ajax_preprocess_views_slideshow(&$vars) {
  /**
   * @todo Why is this function called twice on initial load?
   */
  //xdebug_break();
  if (!_is_views_slideshow_ajax($vars['view'])) {
    return;
  }

  $options = $vars['options'];
  $rows = $vars['rows'];
  $view = $vars['view'];
  $vss_id = $view->name . '-' . $view->current_display;

  /**
   * @todo The 'ajax' param is currently redundant, b/c the Ajax URL for Views is distinct.
   * Can test on $_GET['q'] instead.
   */
  if ($_REQUEST['ajax']) {
    global $is_ajax_authcache;
    $is_ajax_authcache = true;
    drupal_json(array('display' => _views_slideshow_ajax_no_display_section($view, $rows, $vss_id, $options['mode'], (int)$_GET['page']), 'slideshow_count' => $view->total_rows));
    exit;
  }
  
  // "formal" name, in camel case.
  $settings['mode_name'] = "viewsSlideshow{$options['mode_name']}";
  
  // Name in CSS selector format for use in DOM
  $settings['mode'] = $options['mode'];

  /*
   * Should correspond to to the full id as set in
   * template_preprocess_views_slideshow_mode().
   * Assumes contrib module follows the same pattern as
   * views_slideshow_singleframe
   */
  $settings['slideshow_full_id'] = "#{$options['mode']}_main_{$vss_id}";
  /*
   * The class used by views to distinguish the views that have
   * "use ajax" enabled.
   */
  $settings['slideshow_view_dom_id'] = ".view-dom-id-{$vars['id']}";

  global $base_path;
  $settings['ajax_url'] = "{$base_path}views/ajax?view_name={$view->name}&view_display_id={$view->current_display}&ajax=1&page=";

  /*
   * To simulate paging for the JSON calls.
   */
  $settings['slideshow_count'] = $view->total_rows;
  $settings['current_item'] = 0;
  $settings['first_cycle'] = TRUE;

  drupal_add_js(array('viewsSlideshowAjax' => array($settings['slideshow_full_id'] => $settings)), 'setting');
  $module_path = drupal_get_path('module', 'views_slideshow_ajax');
  drupal_add_js($module_path . '/views_slideshow_ajax.js', 'module');
}


/**
 * Helper function to abstract the themed rendition of a single slide.
 * 
 * @param unknown_type $view
 * @param unknown_type $rows
 * @param unknown_type $vss_id
 * @param unknown_type $mode
 */
function _views_slideshow_ajax_no_display_section($view, $rows, $vss_id, $mode, $count = 0) {
  // Should be Ajax call, so assume there's one row.
  return theme($mode . '_no_display_teaser', $rows, $vss_id, $count);
}

/**
 * Helper function to determine whether a given view should be treated as a
 * views_slideshow_ajax instance.
 * 
 * @param $view
 *   A Views view object.
 *   
 * @return
 *   TRUE if the view settings indicate it should be treated as a
 *   views_slideshow_ajax instance, FALSE otherwise.
 */
function _is_views_slideshow_ajax(&$view) {
  return $view->display_handler->get_option('use_ajax') && $view->display_handler->get_option('style_plugin') == 'slideshow';
}
